import Component from '@ember/component';
import { computed } from '@ember/object';
import fitty from 'fitty';

export default Component.extend({

  classNames: ['measure-box'],
  classNameBindings: ['beatSchemaClass'],

  beatSchemaClass: computed('beatSchema', function() {
    return 'measure-box-beatschema-' + this.get('beatSchema');
  }),

  maxFontSizePerSchema: Object.freeze({
    '4': [26],
    '2-2': [19, 19],
    '2-1-1': [19, 16, 16],
    '1-1-2': [16, 16, 19],
    '1-1-1-1': [16, 16, 16, 16]
  }),

  didRender() {
    this.fitChordTexts();
  },

  fitChordTexts() {

    const maxFontSizes = this.maxFontSizePerSchema[this.beatSchema];

    this.element.querySelectorAll('.measure-chord span').forEach((chordTextEl, index) => {
      fitty(
        chordTextEl,
        {
          multiLine: false,
          minSize: 12,
          maxSize: maxFontSizes[index]
        }
      );
    });
  }

});
