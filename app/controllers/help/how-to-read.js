import Controller from '@ember/controller';

export default Controller.extend({

  beatsMeasure_4: Object.freeze(['4 beats']),
  beatsMeasure_2_2: Object.freeze(['2 beats', '2 beats']),
  beatsMeasure_2_1_1: Object.freeze(['2 beats', '1 beat', '1 beat']),
  beatsMeasure_1_1_2: Object.freeze(['1 beat', '1 beat', '2 beats']),
  beatsMeasure_1_1_1_1: Object.freeze(['1 beat', '1 beat', '1 beat', '1 beat']),

  orderMeasure_4: Object.freeze(['1st']),
  orderMeasure_2_2: Object.freeze(['1st', '2nd']),
  orderMeasure_2_1_1: Object.freeze(['1st', '2nd', '3rd']),
  orderMeasure_1_1_2: Object.freeze(['1st', '2nd', '3rd']),
  orderMeasure_1_1_1_1: Object.freeze(['1st', '2nd', '3rd', '4th']),

  repititionMeasureChord: Object.freeze(['C']),
  repititionMeasureRepitionSign: Object.freeze(['%']),

  restMeasureChord: Object.freeze(['REST']),

  alternativeBassNoteMeasureChord: Object.freeze(['E7/B'])

});
