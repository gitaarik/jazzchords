import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | help/how-to-read', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let controller = this.owner.lookup('controller:help/how-to-read');
    assert.ok(controller);
  });
});
