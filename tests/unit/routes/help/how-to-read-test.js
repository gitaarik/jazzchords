import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | help/how-to-read', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:help/how-to-read');
    assert.ok(route);
  });
});
